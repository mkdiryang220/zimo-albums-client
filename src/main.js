import router from 'routerConfig'
import App from './App.vue';
import store from 'store';
import '@/assets/nav.css';
import '@/assets/js/photo-preview/skin.css';

// ElementUI.install(Vue)
Vue.component('footer-copyright', {
    template: '<p class="footer-msg">©2018 zimobaobao <a href="http://www.miibeian.gov.cn" target="_blank">鄂ICP备18016558号</a></p>'
});
// 瀑布流组件
Vue.component('waterfall', Waterfall.waterfall);
Vue.component('waterfall-slot', Waterfall.waterfallSlot);

// 放大组件
Vue.use(vuePhotoPreview, { fullscreenEl: false})
Vue.use(window.VueVideoPlayer)

new Vue({
    router,
    store,
    el: "#app",
    render: h => h(App)
})
