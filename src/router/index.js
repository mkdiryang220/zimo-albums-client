
// import Vue from 'vue'
// import VueRouter from 'vue-router'

// Vue.use(VueRouter)
// 顶部导航条
let navbar = r => require.ensure([], () => r(require('@/components/public/navbar')), 'navbar')
let home = r => require.ensure([], () => r(require('@/views/home')), 'home')
let index = r => require.ensure([], () => r(require('@/views/index/index')), 'index')

// 左边栏
let aside = r => require.ensure([], () => r(require('@/components/public/leftNav')), 'aside')

// 页面路由
let login = r => require.ensure([], () => r(require('@/views/login')), 'login')
let album = r => require.ensure([], () => r(require('@/views/album/albumComp')), 'album')
let photos = r => require.ensure([], () => r(require('@/views/album/photoComp')), 'photos')
let videos = r => require.ensure([], () => r(require('@/views/video/videoComp')), 'videos')

// 技术栈
let flipping = r => require.ensure([], () => r(require('@/views/technologyStack/flipping_3d')), 'flipping');
let editPhoto = r => require.ensure([], () => r(require('@/views/technologyStack/editPhoto')), 'editPhoto');
let uploadPhoto = r => require.ensure([], () => r(require('@/views/technologyStack/uploadPhoto')), 'uploadPhoto');

let router = new VueRouter({
  routes: [
    {
      path: '/', 
      name: "home", 
      components: {
        default: home
      },
      meta: {
        type: 'home'
      },
      redirect: '/index',
      children: [
        {
          path: '/index',
          name: "Index",
          components: {
            default: index,
            top: navbar,
          }
        }
      ]
    },
    {
      path: '/allFiles',
      redirect: '/album',
      components: {
        default: home
      },
      meta: {
        type: 'allFiles'
      },
      children: [
        {
          path: '/album',
          name: "相册",
          components: {
            default: album,
            top: navbar,
            aside: aside
          },
          meta: {
            type: 'album',
            iconCls: 'el-icon-menu',
            parent: 'allFiles',
            leaf: true,
            menuShow: true,
          }
        }, {
          path: '/album/:key',
          name: "photo",
          components: {
            default: photos,
            top: navbar,
            aside: aside
          },
          meta: {
            type: 'photo',
            iconCls: 'el-icon-menu',
            parent: 'allFiles',
            leaf: true,
            menuShow: false,
          }
        }, {
          path: '/video',
          name: "视频",
          components: {
            default: videos,
            top: navbar,
            aside: aside
          },
          meta: {
            type: 'video',
            iconCls: 'el-icon-menu',
            parent: 'allFiles',
            leaf: true,
            menuShow: true,
          }
        }
      ]
    },
    {
      path: '/technologyStack',
      name: "技术栈",
      redirect: '/flipping',
      components: {
        default: home
      },
      meta: {
        type: 'technologyStack'
      },
      children: [
        {
          path: "/flipping",
          name: "3d翻转",
          components: {
            default: flipping,
            top: navbar,
            aside: aside
          },
          meta: { // 不需要侧边栏不必配置meta数据
            type: 'flipping',
            iconCls: 'el-icon-menu',
            parent: 'technologyStack',
            leaf: true,
            menuShow: true,
          }
        },
        {
          path: "/editPhoto",
          name: "编辑照片",
          components: {
            default: editPhoto,
            top: navbar,
            aside: aside
          },
          meta: {
            type: 'editPhoto',
            iconCls: 'el-icon-menu',
            parent: 'technologyStack',
            leaf: true,
            menuShow: true,
          }
        },
        {
          path: "/uploadPhoto",
          name: "上传照片",
          components: {
            default: uploadPhoto,
            top: navbar,
            aside: aside
          },
          meta: {
            type: 'uploadPhoto',
            iconCls: 'el-icon-menu',
            parent: 'technologyStack',
            leaf: true,
            menuShow: true,
          }
        }
      ],
    },
    { 
      path: '/login', 
      name: "登陆", 
      components: {
        default: login,
        top: navbar
      },
      meta: {
        type: 'login'
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.path.startsWith('/login')) {
    window.localStorage.removeItem('u')
    next()
  } else if (to.path.startsWith('/register')) {
    window.localStorage.removeItem('u')
    next()
  } else {
    let user = JSON.parse(window.localStorage.getItem('u'))
    if (!user) {
      next({ path: '/login' })
    } else {
      next()
    }
  }
});
export default router