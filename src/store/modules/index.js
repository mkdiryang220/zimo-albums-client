const app = {
  state: {
    albums: [],
    token: ""
  },
  mutations: {
    
    setToken: (state, token) => {
      state.token = token;
    },
    zoomImageIndex: (state, photo) => {
      state.zoomPhoto = photo;
    }
  },
  actions: {
    setToken({ commit }, token) {
      commit('setToken', token);
    },
    zoomImageIndex: ({ commit }, photo) => {
      commit("zoomImageIndex", photo);
    }
  }
};

export default app;
