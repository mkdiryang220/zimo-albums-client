function sortImages(images) {
  let sortImage = images.sort(function (a, b) {
    return b.create_time - a.create_time
  })
  return sortImage
}

const app = {
  state: {
    albums: [],
    images: [],
    imagePaging: [],
    pageSize: 16,
    pageCount: 1,

    zoomPhoto: null,
    state: false,
    leftNavState: 'home',
    collapsed: false
  },
  mutations: {
    setAlbums: (state, albums) => {
      state.albums = albums
    },
    deleteAlbum: (state, id) => {
      state.albums = state.albums.filter(function (album) {
        return album.album_id !== id
      })
    },
    addAlbum: (state, album) => {
      state.albums.unshift(album)
    },

    setImages: (state, images) => {
      state.images = images
    },
    setImagePaging: (state, pageCount) => {
      let sortImage = sortImages(state.images);
      let imagePaging = sortImage.slice(pageCount * state.pageSize, pageCount * state.pageSize + state.pageSize);
      state.imagePaging = imagePaging;
      state.pageCount = pageCount + 1;
    },
    deleteImage: (state, params) => {
      let images = state.images.filter(function (item) {
        return item.photo_id !== params.id
      })
      state.images = images
      let pageCount = params.pageCount - 1
      let imagePaging = state.images.slice(pageCount * state.pageSize, pageCount * state.pageSize + state.pageSize);
      state.imagePaging = imagePaging;
    },
    addImage: (state, image) => {
      state.images.unshift(image);
      let pageCount = state.pageCount - 1

      let imagePaging = state.images.slice(pageCount * state.pageSize, pageCount * state.pageSize + state.pageSize);
      state.imagePaging = imagePaging;
    },
    clearImage: (state) => {
      state.images = [];
      state.imagePaging = [];
      state.pageCount = 1;
    },
    // 放大
    zoomImageIndex: (state, photo) => {
      state.zoomPhoto = photo;
    },
  },
  actions: {
    setAlbums: ({ commit }, albums) => {
      commit('setAlbums', albums);
    },
    deleteAlbum: ({ commit }, id) => {
      commit('deleteAlbum', id);
    },
    addAlbum: ({ commit }, album) => {
      commit('addAlbum', album);
    },

    setImage: ({ commit }, data) => {
      commit("setImages", data.images);
      commit("setImagePaging", data.pageCount);
      return
    },
    deleteImage: ({ commit }, params) => {
      commit("deleteImage", params);
    },
    addImage: ({ commit }, image) => {
      commit("addImage", image);
    },
    clearImage: ({ commit }) => {
      commit("clearImage");
    },
    // 放大
    zoomImageIndex: ({ commit }, photo) => {
      commit("zoomImageIndex", photo);
    }
  }
};

export default app;
