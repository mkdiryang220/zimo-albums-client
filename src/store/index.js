import Vuex from 'vuex';
import index from './modules/index';
import files from './modules/files';
import gallery from './modules/gallery';
import getters from './getters';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    index,
    files,
    gallery
  },
  getters
});

export default store;
