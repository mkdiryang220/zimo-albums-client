/**   
 * api接口统一管理
 */
import { get, post } from './http'
import { loginOut } from '../common/common.js'
export const loginServer = p => post('/api/login', p);                   // 登陆
export const albumServer = p => post('/api/getalbums', p);               // 查找所有相册
export const albumPhotoServer = p => post('/api/getalbumphotos', p);     // 查找相册内的所有照片
export const getAllPhotoByUser = p => post('/api/getallphotobyuser', p); // 获取用户所有照片
export const addAlbumServer = p => post('/api/addAlbum', p);             // 添加相册
export const deleteAlbumServer = p => post('/api/deletealbum', p);       // 删除相册
export const deletePhotoServer = p => post('/api/deletephoto', p);       // 删除照片
export const uploadPhotoServer = (p, headers) => post('/api/upload', p, headers); // 上传照片
export const getAllVideosByUser = p => post('/api/getvideos', p); // 获取用户所有视频

export function loginOutTarget() {
  let out = loginOut();
  if (out) {
    location.herf = location.origin + "login"
  }
  return out
}