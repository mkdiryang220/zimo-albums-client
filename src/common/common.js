// cache不存在或者时间存在长于1小时清空缓存。
export function loginOut() {
  let u = localStorage.getItem("u")
  let out = false;

  if (u) {
    let timer = Number(JSON.parse(u).d);
    let newData = new Date().getTime()
    if (newData - timer > 1000 * 60 * 60) {
      localStorage.removeItem("u")
      out = true
    }
  } else {
    out = true
  }

  return out
}

export function getToken(){
  let u = localStorage.getItem('u')
  if(u){
    let uid = JSON.parse(u).i
    return uid
  }else{
    return
  }
}

/**
 * 分割 [1,2,3,4,5,6,7] 为 [[1,2,3],[4,5,6]] 或指定的其他块
 * @param {数据: Array} data 
 */
export function getSlice(data, count){
  let newData = []
  if (data.length <= count) {
    newData.push(data);
    return newData
  }
  let arrSlice = []
  for (let i = 0; i < data.length; i++) {
    const e = data[i];
    arrSlice.push(e);
    if ((i + 1) % count === 0) {
      newData.push(arrSlice);
      arrSlice = [];
    }
    if ((i + 1) === data.length && arrSlice.length) {
      newData.push(arrSlice);
    }
  }
  return newData
}

export function changeTheDate(date){
  let d = moment(new Date(Number(date))).format('YYYY-MM-DD hh:mm:ss');
  return d
}