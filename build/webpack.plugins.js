const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const cleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = [
  new cleanWebpackPlugin(["dist"]),
  // make sure to include the plugin for the magic
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: process.env.NODE_ENV === 'development' ? '"development"' : '"production"'
    }
  }),
  // 处理 .vue 文件
  new VueLoaderPlugin(),
  new HtmlWebpackPlugin({
    title: "子墨相册",
    filename: 'index.html',
    template: 'index.html',
    favicon: path.resolve('./src/assets/favicon.ico')
  }),
  // 全局暴露第三方库
  new webpack.ProvidePlugin({
    _: 'lodash', //所有页面都会引入 _ 这个变量，不用再import引入
    api: path.join(__dirname, 'src/common/api.js'),
    ElementUI: 'element-ui'
  }),
  new CopyWebpackPlugin([ // 复制插件
    { from: path.join(__dirname,'../src/assets'), to:  path.join(__dirname,'../dist') }
  ])
]