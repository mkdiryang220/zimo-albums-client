
// const copyWebpackPlugin = require("copy-webpack-plugin")
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = function (config) {
  // 生产环境下 vue 文件中 style 标签合并打包 
  config.plugins.push(
    new ExtractTextPlugin({
      filename: (getPath) => {
        return getPath('css/[name].css').replace('css/js', 'css');
      },
      allChunks: true
    })
  )

  // 生产环境下css文件处理
  config.module.rules.push({
    test: /\.css$/,
    loader: ExtractTextPlugin.extract({ 
      fallback: 'style-loader',
      use: 'css-loader' 
    }),
    exclude: /node_modules/,
  })

  return config
}