//开发环境 生产环境共用rules
module.exports = {
  rules: [
    // {
    //   test: /\.css$/,
    //   use: [
    //     'style-loader',
    //     'css-loader'
    //   ]
    // },
    {
      test: /\.vue$/,
      exclude: /node_modules/,
      loader: 'vue-loader'
    },
    {
      test: /\.jsx$/,
      loader: 'bable-loader',
      exclude: /node_modules/
    },
    {
      test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000
      }
    },
    {
      test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
      loader: 'url-loader',
      options: {
        limit: 10000
      }
    },
    {
      test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
      loader: 'file-loader',
      options: {
        limit: 10000
      }
    },
    {
      test: /\.less$/,
      loader: 'style-loader!css-loader!less-loader'
    },
  ]
}