const path = require('path');
const webpackRules = require('./build/webpack.rules')
const plugins = require('./build/webpack.plugins')
const dev = require('./build/webpack.dev')
const pro = require('./build/webpack.pro')
const isDev = process.env.NODE_ENV === 'development';

function resolve(dir) {
    return path.join(__dirname, dir)
}

let commonlib = {
    '@': resolve('src'),
    'assets': resolve('src/assets'),
    'components': resolve('src/components'),
    'config': resolve('config.js'),
    'store': resolve('src/store/index.js'),
    'api': resolve('src/common/api.js'),
    'common': resolve('src/common/common.js'),
    'routerConfig': resolve('src/router/index.js')
}

let config = {
    target: 'web',
    performance: {
        hints: false
    },
    entry: {
        main: resolve('src/main.js')
    },
    externals: {
        'vue': 'Vue',    
        'element-ui': 'ElementUI',
        'Vuex': 'vuex',
        'moment': 'moment',
        'VueLazyload': 'VueLazyload',
        'BootstrapVue': 'BootstrapVue'
    },
    output: {
        path: resolve('dist'),
        filename: '[name].[hash].bundle.js',
        chunkFilename: '[name].[chunkhash].js',
    },
    resolve: {
        extensions: ['.js', '.vue', '.less', '.css', '.scss'],
        alias: commonlib
    },
    plugins: plugins,
    module: webpackRules
}

if (isDev){
    config = dev(config)
} else {
    config = pro(config)
    config.devtool = false
}

module.exports = config